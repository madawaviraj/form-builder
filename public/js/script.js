var elements = [];
$(function () {
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
    $(".drop-zone").sortable({
        revert: true,
        receive: function (e, ui) {
            var fieldType = ui.item[0].classList[2];
            var currentOrder = $('.drop-zone').children().length;
            var labelText = '';
            var element = $(this).data().uiSortable.currentItem;
            itemModify(element);
            var appendData = "<span class='col-md-1 remove-btn' onclick=removeComponent('" + element[0].id + "')>" +
                "<i class='far fa-times-circle'></i></span>";
            element.children('div').append(appendData);
            element.children('div').children('div').removeClass('col-md-8').addClass('col-md-6');
        },
        update: function (event, ui) {
            elements = [];
            console.log(elements);
            $('.drop-zone .component .input-group').on('click', "label.control-label", function () {
                var lbl = $(this);
                var lblText = lbl.text();
                var txt = $('<input type="text" class="editable-label-text" value="' + lblText + '" />');
                lbl.replaceWith(txt);
                txt.focus();
                txt.blur(function () {
                    txt.replaceWith(lbl);
                }).keydown(function (evt) {
                    if (evt.keyCode == 13) {
                        var labelText = $(this).val();
                        lbl.text(labelText);
                        txt.replaceWith(lbl);
                        // console.log(no);
                        var id = lbl.parents('.component').attr('id');
                        var key;
                        for (var i = 0; elements.length > i; i++) {
                            if (elements[i]['id'] === id) {
                                var labelValue = $('#' + id + ' div label').text();
                                elements[i]['label'] = labelValue;
                            }
                        }
                    }
                });
            });
            $('.drop-zone').children().each(function (e) {
                var fieldType = $(this).attr('class').split(' ')[2];
                var id = $(this).attr('id');
                var label = $('#' + id + ' div label').text();
                if (label === 'Rename label and press enter') {
                    var label = 'Default Text';
                }
                orderData = {
                    "type": fieldType,
                    "id": id,
                    "order": ($(this).index() + 1),
                    "label": label
                };
                elements.push(orderData);
            });
        }
    });
    $('.component').draggable({
        connectToSortable: '.drop-zone',
        helper: "clone",
        revert: true
    });
    $('#btn-submit').on('click', function (e) {
        var formTitle = $('.drop-zone-wrapper .box h3').text();
        if (elements.length > 0) {
            $.ajax({
                type: 'POST',
                url: '/form-store',
                data: {
                    "title": formTitle,
                    "elements": elements
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                success: function (data) {
                    var addendData = '';
                    if (data.success === 'true') {
                        addendData = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                            'Form Submission successfull.' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>';
                    } else {
                        addendData = '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                            'Form Submission failed. Please try again.' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>';
                    }
                    $('.form-wrapper').prepend(addendData);
                }
            });
        } else {

        }
    });
    $('.drop-zone-wrapper .box').on('click', "h3.box-title", function () {
        var lbl = $(this);
        var lblText = lbl.text();
        var txt = $('<input type="text" class="editable-label-text" value="' + lblText + '" />');
        lbl.replaceWith(txt);
        txt.focus();
        txt.blur(function () {
            txt.replaceWith(lbl);
        }).keydown(function (evt) {
            if (evt.keyCode == 13) {
                var labelText = $(this).val();
                lbl.text(labelText);
                txt.replaceWith(lbl);
            }
        });
    });
});
function itemModify(item) {
    var elemCount = $('.drop-zone').children().length;
    var itemLabel = item.children().children('label');
    item.attr('id', 'field-' + elemCount);
    itemLabel.text('Rename label and press enter');
}
function removeComponent(itemId) {
    $('#' + itemId).remove();
}