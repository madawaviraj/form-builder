<header class="main-header">
    <a href="#" class="logo">
    </a>
    <nav class="navbar navbar-static-top">
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Links</li>
            <li><a href="{{ route('form.index') }}"><i class="fas fa-link"></i> <span>Form List</span></a></li>
            <li><a href="{{ route('form.create') }}"><i class="fas fa-book"></i> <span>Create Form</span></a></li>
        </ul>
    </section>
</aside>