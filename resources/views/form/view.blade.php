@extends('layouts.master')

@section('content')
<!-- start enterprise registration form -->
<div class="content-wrapper">
    <section class="content-header">
        <div>
            <h1>
                {{ $form->form_name }}
            </h1>
        </div>
        <div class="box box-primary col-md-12">
            <div class="box-body no-padding col-md-12">
                <form action="#" method="post">
                    <div class="col-md-12 component text form-group">
                        <div class="row mt-5">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    @foreach($formFields as $formField)
                                        @if($formField->field_type == 'text')
                                            <div class="form-group col-md-12">
                                                <label for="inputText" class="col-md-4 control-label">{{ $formField->field_name }}</label>
                                                <div class="input-group col-md-8">
                                                    <input type="text" class="form-control" placeholder="Placeholder">
                                                </div>
                                            </div>
                                        @elseif($formField->field_type == 'text-area')
                                            <div class="form-group col-md-12">
                                                <label for="inputText" class="col-md-4 control-label">{{ $formField->field_name }}</label>
                                                <div class="input-group col-md-8">
                                                    <textarea class="form-control" rows="3" placeholder="Placeholder"></textarea>
                                                </div>
                                            </div>
                                        @elseif($formField->field_type == 'datepicker')
                                            <div class="form-group col-md-12">
                                                <label for="inputText" class="col-md-4 control-label">{{ $formField->field_name }}</label>
                                                <div class="input-group col-md-8">
                                                    <input type="text" class="form-control pull-right" id="datepicker">
                                                </div>
                                            </div>
                                        @elseif($formField->field_type == 'select-box')
                                            <div class="form-group col-md-12">
                                                <label for="inputText" class="col-md-4 control-label">{{ $formField->field_name }}</label>
                                                <div class="input-group col-md-8">
                                                    <select class="form-control">
                                                        <option>option 1</option>
                                                        <option>option 2</option>
                                                        <option>option 3</option>
                                                        <option>option 4</option>
                                                        <option>option 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                        @elseif($formField->field_type == 'select-box')
                                            <div class="form-group col-md-12">
                                                <label for="inputText" class="col-md-4 control-label">{{ $formField->field_name }}</label>
                                                <div class="input-group col-md-8">
                                                    <div class="iradio_minimal-blue checked">
                                                        <input type="radio" name="same" class="minimal">
                                                    </div>
                                                    <div class="iradio_minimal-blue">
                                                        <input type="radio" name="same" class="minimal">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="address">
                                                <h5></h5>
                                            </label>
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-success mr-3" name="submit_sms" id="submit_sms">Send</button>
                                                <button type="submit" class="btn btn-danger" name="submit_sms" id="submit_sms">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection