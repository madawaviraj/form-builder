@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div>
            <h1>
                Form List
            </h1>
        </div>
        <div class="box box-primary col-md-12">
            <div class="row" id="basic_info">    
                <div class="col-lg-12" >
                    <div class="card-content">
                        <table id="supplier" class="table table-striped table-bordered" style="text-align: center;">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Form Name</th>
                                    <th width="20%">Field Count</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($returnData))
                                    @foreach($returnData as $data)
                                    <tr>
                                        <th width="5%">{{ $loop->iteration }}</th>
                                        <th width="20%">{{ $data->form_name }}</th>
                                        <th width="20%">{{ $data->fields }}</th>
                                        <th width="5%"><a href="{{url('/form/'.$data->id)}}" class="btn btn-block btn-success btn-flat btn-sm" disabled="disabled">View</a></th>
                                    </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <th width="20%" colspan=4>You have not created any form.</th>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection