@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div>
            <h1>
                Create Your Form
            </h1>
        </div>
        <div class="box box-primary col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">Form Elements</h3>
            </div>
            <div class="box-body form-wrapper no-padding col-md-12">
                <div class="col-md-6 component-list float-left">
                    <div class="col-md-12 component text form-group">
                        <div class="input-group col-md-12">
                            <label for="inputText" class="col-md-4 control-label">Text Input</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Placeholder">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 component text-area form-group">
                        <div class="input-group col-md-12">
                            <label for="inputText" class="col-md-4 control-label">Text Area</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="3" placeholder="Placeholder"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 component select-box form-group">
                        <div class="input-group col-md-12">
                            <label for="inputText" class="col-md-4 control-label">Combo Box</label>
                            <div class="col-md-8">
                                <select class="form-control">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                    <option>option 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 component datepicker form-group">
                        <div class="input-group col-md-12 date">
                            <label for="inputText" class="col-md-4 control-label">Date Picker</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 component radio-btn form-group">
                        <div class="input-group col-md-12">
                            <label for="inputText" class="col-md-4 control-label">Radio Button</label>
                            <div class="col-md-8">
                                <div class="iradio_minimal-blue checked">
                                    <input type="radio" name="same" class="minimal">
                                </div>
                                <div class="iradio_minimal-blue">
                                    <input type="radio" name="same" class="minimal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 drop-zone-wrapper float-right">
                    <div class="box box-default">
                        <h3 class="box-title">Click here to Rename form and press enter</h3>
                        <div class="box-body drop-zone with-border">
                        </div>
                        <div class="col-md-12 form-group float-right">
                            <div class="input-group col-md-12">
                                <input type="button" class="btn btn-primary mt-3" id="btn-submit" value="Submit" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection