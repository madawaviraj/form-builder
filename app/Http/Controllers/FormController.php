<?php

namespace App\Http\Controllers;

use App\Form;
use App\FormFields;
use App\FieldDetails;
use DB;
use Auth;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function __construct()
	{
		$this->middleware(['auth']);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;
        $returnData = DB::table("forms")
            ->where('user_id', $user)
            ->join('form_fields', 'form_fields.form_id', '=', 'forms.id')
            ->select(DB::raw('count(form_fields.id) as fields'), 'forms.*')
            ->groupBy("forms.id")
            ->get();
        
        return view('form.index', compact('returnData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->title;
        $elements = $request->elements;

        $formCount = Form::count();
        $user = Auth::user()->id;
        $form = new Form();
        $form->form_name = $title;
        $form->user_id = $user;
        $form->save();
        $formId = $form->id;

        $formFields = array();
        foreach ($elements as $element) {
            $fields = array('form_id' => $formId, 'field_name' => $element['label'], 'field_type' => $element['type'], 'order' => $element['order']);
            array_push($formFields, $fields);
        }
        $formFields = FormFields::insert($formFields);

        if($formFields){
            return response()->json(array('success' => 'true'));
        } else {
            return response()->json(array('success' => 'false'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        $formFields = FormFields::where('form_id', $form->id)
            ->orderBy('order')
            ->get();
        return view('form.view', compact('form', 'formFields'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }
}
